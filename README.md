# XMAS TREE

Version:        0.1 - 20171113

Author:         Darian Cabot

Copyright:      (c) 2017 Cabot Technologies

License:        MIT License (see LICENSE.txt)

## Visit us

* Web: https://cabottechnologies.com/projects/christmas-tree/
* Git: https://bitbucket.org/cabot_tech/xt-firmware
* Twitter: https://twitter.com/cabot_tech
